//
//  SoraProperty.h
//  HTL
//
//  Created by Robert Bu on 9/2/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef HTL_SoraProperty_h
#define HTL_SoraProperty_h

#include "property/SoraProperty.h"
#include "property/SoraPropertyHolder.h"
#include "property/SoraDynRTTIClass.h"

#endif
