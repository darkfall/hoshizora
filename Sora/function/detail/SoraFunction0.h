//
//  SoraFunction0.h
//  zeromq_client
//
//  Created by Robert Bu on 8/16/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef SoraFunction0_h
#define SoraFunction0_h

#define SORA_FUNCTION_NUM_ARGS 0
#include "SoraFunctionInclude.h"
#undef SORA_FUNCTION_NUM_ARGS

#endif
