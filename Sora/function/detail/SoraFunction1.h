//
//  SoraFunction1.h
//  zeromq_client
//
//  Created by Robert Bu on 8/15/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef SoraFunction1_h
#define SoraFunction1_h

#define SORA_FUNCTION_NUM_ARGS 1
#include "SoraFunctionInclude.h"
#undef SORA_FUNCTION_NUM_ARGS

#endif
