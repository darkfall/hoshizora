//
//  SoraFunction3.h
//  zeromq_client
//
//  Created by Robert Bu on 8/16/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef SoraFunction9_h
#define SoraFunction9_h

#define SORA_FUNCTION_NUM_ARGS 9
#include "SoraFunctionInclude.h"
#undef SORA_FUNCTION_NUM_ARGS

#endif
