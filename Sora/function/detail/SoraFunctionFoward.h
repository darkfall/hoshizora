//
//  SoraFunctionFoward.h
//  zeromq_client
//
//  Created by Robert Bu on 8/16/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef SoraFunctionFoward_h
#define SoraFunctionFoward_h

namespace sora {
    
    template<typename signature>
    class SoraFunction;
    
}

#endif
