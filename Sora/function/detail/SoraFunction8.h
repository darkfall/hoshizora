//
//  SoraFunction3.h
//  zeromq_client
//
//  Created by Robert Bu on 8/16/11.
//  Copyright 2011 GameMaster Studio(Project Hoshizora). All rights reserved.
//

#ifndef SoraFunction8_h
#define SoraFunction8_h

#define SORA_FUNCTION_NUM_ARGS 8
#include "SoraFunctionInclude.h"
#undef SORA_FUNCTION_NUM_ARGS

#endif
