/*
 *  SoraiOSGLRenderer.h
 *  Plugin Concept
 *
 *  Created by griffin clare on 1/26/11.
 *  Copyright 2010 Griffin Bu(Project L). All rights reserved.
 *
 */

#ifndef _SORA_IOSGL_RENDERER_H_
#define _SORA_IOSGL_RENDERER_H_

#include "SoraRenderSystem.h"
#include "SoraiOSDeviceHelper.h"

#ifdef OS_IOS
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>
#endif

namespace sora {
    
    class SoraRenderTargetiOSGL;

	class SoraiOSGLRenderer: public SoraRenderSystem {
	public:
		SoraiOSGLRenderer();
		~SoraiOSGLRenderer();
		
		bool update();
		
		void beginScene(uint32 color=0, SoraHandle target=0, bool clear=true);
		void endScene();
		
		void beginFrame();
		void endFrame();
		
		void shutdown();
		bool isActive();
		
		void start(SoraTimer* timer);
		
		SoraHandle createTarget(int width, int height, bool zbuffer=true);
		void        freeTarget(SoraHandle t);
		SoraHandle getTargetTexture(SoraHandle t);
		
		SoraWindowHandle createWindow(SoraWindowInfoBase* windowInfo);
		void setWindowSize(int32 w, int32 h);
		void setWindowTitle(const StringType& title);
		void setWindowPos(int32 px, int32 py);
		void setFullscreen(bool flag);
		bool isFullscreen();
		
		SoraTexture* createTexture(const StringType& sTexturePath, bool bMipmap=false);
		SoraTexture* createTextureWH(int w, int h);
		SoraTexture* createTextureFromMem(void* ptr, uint32 size, bool bMipmap=false);
		SoraTexture* createTextureFromRawData(unsigned int* data, int32 w, int32 h);
		
		uint32*		 textureLock(SoraTexture*);
		void		 textureUnlock(SoraTexture*);
		void		 releaseTexture(SoraTexture* tex);
		
		void renderQuad(SoraQuad& quad);
		void renderTriple(SoraTriple& trip);
		void renderWithVertices(SoraTexture* tex, int32 blendMode, SoraVertex* vertices, uint32 vsize, RenderMode mode);
		
		void setClipping(int32 x=0, int32 y=0, int32 w=0, int32 h=0);
		void setTransform(float x=0.f, float y=0.f, float dx=0.f, float dy=0.f, float rot=0.f, float hscale=1.f, float vscale=1.f);
		void setTransformWindowSize(float w, float h);
        void setViewPoint(float x=0.f, float y=0.f, float z=0.f);
		
		SoraHandle getMainWindowHandle();
		SoraWindowInfoBase* getMainWindow();
		
        SoraShaderContext* createShaderContext();
		void attachShaderContext(SoraShaderContext* context);
		void detachShaderContext();
		
		StringType videoInfo();
		SoraHandle getVideoDeviceHandle();
        void setVerticalSync(bool flag);
		
		void flush();
        
        void snapshot(const StringType& path);
		void onExtensionStateChanged(int32 extension, bool state, int32 param);
        
        void renderLine(float x1, float y1, float x2, float y2, uint32 color, float width, float z=0.f);
		void renderBox(float x1, float y1, float x2, float y2, uint32 color, float width, float z=0.f);
        void fillBox(float x1, float y1, float x2, float y2, uint32 color, float z=0.f);

        void setIcon(const StringType& icon);
        void setCursor(const StringType& cursor);
        
        void setOrientation(iOSOrientation por);
        iOSOrientation getOrientation() const;
        
        void tranlatePointToGL(float* x, float* y);
        
        void getDesktopResolution(int* w, int* h);
        void setQueryVideoModeCallback(QueryVideoMode func);
        
        void switchTo2D();
        void switchTo3D();
        
        void setTransformMatrix(const SoraMatrix4& matrix);
        void multTransformMatrix(const SoraMatrix4& matrix);
        SoraMatrix4 getTransformMatrix() const;
        
        void setProjectionMatrix(const SoraMatrix4& matrix);
        SoraMatrix4 getProjectionMatrix() const;
        
        void setRenderState(RenderStateType, RenderStateParam);
        RenderStateParam getRenderState(RenderStateType) const;
        
	private:
		void applyTransform();
		void bindTexture(SoraTexture* tex);
		
		inline int32 _modeToGLMode(int32 mode);
		inline void _glInitialize();
		inline void _glEndFrame();
		inline void _glBeginFrame();
		inline void _glBeginScene(uint32 color, SoraHandle target, bool clear);
		inline void _glEndScene();
		inline int32 _glTextureGetWidth(SoraHandle tex, bool bOriginal=false);
		inline int32 _glTextureGetHeight(SoraHandle tex, bool bOriginal=false);
		inline void _glSetProjectionMatrix(int32 w, int32 h);
		inline void _glSetBlendMode(int32 mode);
		
		// checks if opengl 2.0 is available
		inline bool _glVersionCheck();
		// checks if glsl is avaiable
		inline bool _glShaderCheck();
		const char* _glGetShaderLog(GLuint shader);
		
		inline bool _glCheckError();
		
		SoraWindowInfoBase* mainWindow;
		int32 windowWidth;
		int32 windowHeight;
		
		struct _SoraOGLWindowInfo {
			float x, y, z;
			float dx, dy;
			float rot;
			float hscale, vscale;
			int32 width, height;
			
			_SoraOGLWindowInfo(): hscale(1.f), vscale(1.f), x(0.f), y(0.f), z(0.f), dx(0.f), dy(0.f), rot(0.f), width(0), height(0) {}
		};
		_SoraOGLWindowInfo _oglWindowInfo;
		
		int32 CurBlendMode;
		int32 CurDrawMode;
				
		std::list<SoraRenderTargetiOSGL*> liTargets;
		SoraRenderTargetiOSGL* pCurTarget;
		
		GLuint mCurrTexture;

		SoraShaderContext* currShader;

		int iFrameStart;
        
        iOSOrientation mOrientation;
	};
} // namespace sora

#endif