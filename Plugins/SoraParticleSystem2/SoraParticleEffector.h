//
//  SoraParticleEffector.h
//  Sora
//
//  Created by Robert Bu on 8/29/11.
//  Copyright 2011 Robert Bu(Project Hoshizora). All rights reserved.
//

#ifndef Sora_SoraParticleEffector_2_h
#define Sora_SoraParticleEffector_2_h

#include "SoraUncopyable.h"

namespace sora {
    
    struct SoraParticleEffector: SoraUncopyable {
        
    };
    
} // namespace sora


#endif
