//
//  prerequisites.h
//  Sora
//
//  Created by Ruiwei Bu on 9/4/11.
//  Copyright 2011 Robert Bu(Project Hoshizora). All rights reserved.
//

#ifndef Sora_prerequisites_h
#define Sora_prerequisites_h

#include "SoraPlatform.h"
#include "SoraUncopyable.h"
#include "SoraMemoryBuffer.h"

namespace sora {
    
    enum ConfigType {
        CONFIG_XML = 1,
        CONFIG_JSON,
    };
    
} // namespace sora



#endif
